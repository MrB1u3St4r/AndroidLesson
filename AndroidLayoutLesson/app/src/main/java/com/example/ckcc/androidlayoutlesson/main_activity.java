package com.example.ckcc.androidlayoutlesson;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.ckcc.androidlayoutlesson.R;

/**
 * Created by ckcc on 8/25/17.
 */

public class main_activity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_layout);
    }
    void ViewActivity(View view){
        Intent i = new Intent(main_activity.this,next_activity.class);
        startActivity(i);
    }
}
